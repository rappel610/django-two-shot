from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from django.contrib.auth.decorators import login_required
from receipts.forms import CreateReceiptForm, CreateExpenseCategory, CreateAccount

# Create your views here.


@login_required(login_url="login")
def receipt_list(request):
    receipts = Receipt.objects.filter(purchaser=request.user)
    context = {'receipts': receipts}
    return render(request, 'receipts/list.html', context)


@login_required(login_url="login")
def create_receipt(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect('home')
    else:
        form = CreateReceiptForm()
    context = {"form": form}
    return render(request, "receipts/create.html", context)


@login_required(login_url="login")
def expense_category_list(request):
    categories = ExpenseCategory.objects.filter(owner=request.user)
    context = {'categories': categories}
    return render(request, 'categories/list.html', context)


@login_required(login_url="login")
def account_list(request):
    accounts = Account.objects.filter(owner=request.user)
    context = {'accounts': accounts}
    return render(request, 'accounts/list.html', context)


@login_required(login_url="login")
def create_expense_category(request):
    if request.method == "POST":
        form = CreateExpenseCategory(request.POST)
        if form.is_valid():
            category = form.save(False)
            category.owner = request.user
            category.save()
            return redirect('category_list')
    else:
        form = CreateExpenseCategory()
    context = {"form": form}
    return render(request, 'categories/create.html', context)


@login_required(login_url="login")
def create_account(request):
    if request.method == "POST":
        form = CreateAccount(request.POST)
        if form.is_valid():
            account = form.save(False)
            account.owner = request.user
            account.save()
            return redirect('account_list')
    else:
        form = CreateAccount()
    context = {"form": form}
    return render(request, 'accounts/create.html', context)
